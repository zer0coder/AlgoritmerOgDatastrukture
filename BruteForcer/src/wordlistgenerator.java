import java.io.*;
/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    26-02-2015
 * Time:    22:08
 */
public class wordlistgenerator {

    private static char[] chars    = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','X','Y','Z',
            '1','2','3','4','5','6','7','8','9','0'};

    public static void main(String[] args) {
        //wordlistgenerator("test",4);
    }

    public static void wordlistgenerator(String name, int wordlength) {

        String fileName = name;

        try {
            File file = new File(name + ".txt");
            if(!file.exists()) {
                file.createNewFile();
            }
            if(file.exists()) {
                file.delete();
            }

            FileWriter fwriter = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bwriter = new BufferedWriter(fwriter);

            if(wordlength == 0) {
                System.out.println("LENGTH IS ZERO!");
                return;
            }

            final long MAX_WORDS = (long) Math.pow(chars.length, wordlength);
            final int RADIX = chars.length;

            for (long i = 0; i < MAX_WORDS; i++) {
                int[] indices = convertToRadix(RADIX, i, wordlength);
                char[] word = new char[wordlength];
                for (int k = 0; k < wordlength; k++) {
                    word[k] = chars[indices[k]];
                }
                bwriter.write(String.valueOf(word) + "\n");
            }
            bwriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int[] convertToRadix(int radix, long number, int wordlength) {
        int[] indices = new int[wordlength];
        for (int i = wordlength - 1; i >= 0; i--) {
            if (number > 0) {
                int rest = (int) (number % radix);
                number /= radix;
                indices[i] = rest;
            } else {
                indices[i] = 0;
            }
        }
        return indices;
    }
}
