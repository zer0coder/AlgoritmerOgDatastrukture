import java.io.*;
import java.util.List;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.unzip.UnzipUtil;
/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    28-02-2015
 * Time:    19:54
 */
public class Decrypter {

    private static char[] chars    = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','X','Y','Z',
            '1','2','3','4','5','6','7','8','9','0'};

    private static final int BUFF_SIZE = 4096;
    private static String getWord;
    private static decryptTest dT;

    public static void Decrypter(String length, String filepath, int debug) throws IOException {
        ZipInputStream is = null;
        OutputStream os = null;
        int charlength = Integer.parseInt(length);
        int wordlength = 0;
        ////////////DEBUG////////////
        if(debug == 1) {
            dT = new decryptTest();
        }
        /////////////////////////////

        for (int m = 1; m < charlength; m++) {
            wordlength = m;
            final long MAX_WORDS = (long) Math.pow(chars.length, wordlength);
            final int RADIX = chars.length;


            for (long n = 0; n < MAX_WORDS; n++) {
                int[] indices = convertToRadix(RADIX, n, wordlength);
                char[] word = new char[wordlength];
                for (int k = 0; k < wordlength; k++) {
                    word[k] = chars[indices[k]];
                }
                getWord = String.valueOf(word);
                ////////////DEBUG////////////
                if(debug == 1) {
                    dT.setString(String.valueOf(word));
                }
                /////////////////////////////

                try {
                    ZipFile zipFile = new ZipFile(filepath);
                    String destinationPath = "F:\\FRANCISCOS MAPPE\\Universitet\\AlgoritmerOgDatastrukture\\BruteForcer\\src\\ZipTest";

                    if (zipFile.isEncrypted()) {
                        zipFile.setPassword(getWord);
                    }

                    List fileHeaderList = zipFile.getFileHeaders();

                    // Loop through all the fileHeaders
                    for (int i = 0; i < fileHeaderList.size(); i++) {
                        FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                        if (fileHeader != null) {

                            String outFilePath = destinationPath + System.getProperty("file.separator") + fileHeader.getFileName();
                            File outFile = new File(outFilePath);

                            if (fileHeader.isDirectory()) {
                                outFile.mkdirs();
                                continue;
                            }

                            File parentDir = outFile.getParentFile();
                            if (!parentDir.exists()) {
                                parentDir.mkdirs();
                            }

                            is = zipFile.getInputStream(fileHeader);
                            os = new FileOutputStream(outFile);

                            int readLen = -1;
                            byte[] buff = new byte[BUFF_SIZE];

                            //Loop until End of File and write the contents to the output stream
                            while ((readLen = is.read(buff)) != -1) {
                                os.write(buff, 0, readLen);
                            }

                            closeFileHandlers(is, os);
                            UnzipUtil.applyFileAttributes(fileHeader, outFile);

                            //System.out.println("Done extracting: " + fileHeader.getFileName());
                            //System.out.println("Password is: " + getWord);
                            return;
                        } else {
                            System.err.println("fileheader is null. Shouldn't be here");
                        }
                    }
                } catch (Exception e) {
                    //e.getSuppressed();
                    //e.printStackTrace();
                } finally {
                    try {
                        closeFileHandlers(is, os);
                    } catch (IOException e) {
                        //e.getSuppressed();
                        //e.printStackTrace();
                    }
                }
            }
        }
    }

    public String getGetWord() { return getWord; }

    private static void closeFileHandlers(ZipInputStream is, OutputStream os) throws IOException{
        if (os != null) {
            os.close();
            os = null;
        }
        if (is != null) {
            is.close();
            is = null;
        }
    }

    private static int[] convertToRadix(int radix, long number, int wordlength) {
        int[] indices = new int[wordlength];
        for (int i = wordlength - 1; i >= 0; i--) {
            if (number > 0) {
                int rest = (int) (number % radix);
                number /= radix;
                indices[i] = rest;
            } else {
                indices[i] = 0;
            }
        }
        return indices;
    }
}
