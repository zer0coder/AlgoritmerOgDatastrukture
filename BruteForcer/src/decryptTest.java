import java.io.IOException;
/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    01-03-2015
 * Time:    12:15
 */
public class decryptTest {

    private static Decrypter decrypter;
    private String word;
    private static String path;
    private static String length;
    private static int debug;

    public static void main(String[] args) {

        /*
        path = "F:\\FRANCISCOS MAPPE\\Universitet\\AlgoritmerOgDatastrukture\\BruteForcer\\src\\space.zip";
        length = "6";
        debug = 1;
        try {
            decrypter.Decrypter(length, path, debug);
        } catch (IOException e) {
            e.printStackTrace();
        }
        */


        path = args[0];
        length = args[1];
        debug = Integer.parseInt(args[2]);
        try {
            decrypter.Decrypter(length, path, debug);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setString(String str) {
        word = str;
        System.out.println(word);
    }
}
