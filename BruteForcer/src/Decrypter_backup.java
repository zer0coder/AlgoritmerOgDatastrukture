import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.unzip.UnzipUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    28-02-2015
 * Time:    19:54
 */
public class Decrypter_backup {

    private static char[] chars    = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','X','Y','Z',
            '1','2','3','4','5','6','7','8','9','0'};

    private static final int BUFF_SIZE = 4096;
    private static String getWord;

    public static void main(String[] args) throws IOException {
        ExtractZipFiles("test",6);
    }

    private static void ExtractZipFiles(String arg, int charlength) {
        ZipInputStream is = null;
        OutputStream os = null;
        String fileName = arg;
        int wordlength = 0;

        for (int m = 1; m < charlength; m++) {
            wordlength = m;
            final long MAX_WORDS = (long) Math.pow(chars.length, wordlength);
            final int RADIX = chars.length;

            for (long n = 0; n < MAX_WORDS; n++) {
                int[] indices = convertToRadix(RADIX, n, wordlength);
                char[] word = new char[wordlength];
                for (int k = 0; k < wordlength; k++) {
                    word[k] = chars[indices[k]];
                }
                getWord = String.valueOf(word);

                try {
                    // Initiate the ZipFile
                    ZipFile zipFile = new ZipFile("F:\\FRANCISCOS MAPPE\\Universitet\\AlgoritmerOgDatastrukture\\BruteForcer\\src\\space.zip");
                    String destinationPath = "F:\\FRANCISCOS MAPPE\\Universitet\\AlgoritmerOgDatastrukture\\BruteForcer\\src\\ZipTest";

                    // If zip file is password protected then set the password
                    if (zipFile.isEncrypted()) {
                        zipFile.setPassword(getWord);
                    }
                    System.out.println(String.valueOf(word));

                    //Get a list of FileHeader. FileHeader is the header information for all the
                    //files in the ZipFile
                    List fileHeaderList = zipFile.getFileHeaders();

                    // Loop through all the fileHeaders
                    for (int i = 0; i < fileHeaderList.size(); i++) {
                        FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
                        if (fileHeader != null) {

                            //Build the output file
                            String outFilePath = destinationPath + System.getProperty("file.separator") + fileHeader.getFileName();
                            File outFile = new File(outFilePath);

                            //Checks if the file is a directory
                            if (fileHeader.isDirectory()) {
                                //This functionality is up to your requirements
                                //For now I create the directory
                                outFile.mkdirs();
                                continue;
                            }

                            //Check if the directories(including parent directories)
                            //in the output file path exists
                            File parentDir = outFile.getParentFile();
                            if (!parentDir.exists()) {
                                parentDir.mkdirs();
                            }

                            //Get the InputStream from the ZipFile
                            is = zipFile.getInputStream(fileHeader);
                            //Initialize the output stream
                            os = new FileOutputStream(outFile);

                            int readLen = -1;
                            byte[] buff = new byte[BUFF_SIZE];

                            //Loop until End of File and write the contents to the output stream
                            while ((readLen = is.read(buff)) != -1) {
                                os.write(buff, 0, readLen);
                            }

                            //Please have a look into this method for some important comments
                            closeFileHandlers(is, os);

                            //To restore File attributes (ex: last modified file time,
                            //read only flag, etc) of the extracted file, a utility class
                            //can be used as shown below
                            UnzipUtil.applyFileAttributes(fileHeader, outFile);

                            System.out.println("Done extracting: " + fileHeader.getFileName());
                            System.out.println("Password is: " + getWord);
                            return;
                        } else {
                            System.err.println("fileheader is null. Shouldn't be here");
                        }
                    }
                } catch (Exception e) {
                    //e.getSuppressed();
                    //e.printStackTrace();
                } finally {
                    try {
                        closeFileHandlers(is, os);
                    } catch (IOException e) {
                        //e.getSuppressed();
                        //e.printStackTrace();
                    }
                }
            }
        }
    }

    private static void closeFileHandlers(ZipInputStream is, OutputStream os) throws IOException{
        //Close output stream
        if (os != null) {
            os.close();
            os = null;
        }

        //Closing inputstream also checks for CRC of the the just extracted file.
        //If CRC check has to be skipped (for ex: to cancel the unzip operation, etc)
        //use method is.close(boolean skipCRCCheck) and set the flag,
        //skipCRCCheck to false
        //NOTE: It is recommended to close outputStream first because Zip4j throws
        //an exception if CRC check fails
        if (is != null) {
            is.close();
            is = null;
        }
    }

    private static int[] convertToRadix(int radix, long number, int wordlength) {
        int[] indices = new int[wordlength];
        for (int i = wordlength - 1; i >= 0; i--) {
            if (number > 0) {
                int rest = (int) (number % radix);
                number /= radix;
                indices[i] = rest;
            } else {
                indices[i] = 0;
            }
        }
        return indices;
    }
}
