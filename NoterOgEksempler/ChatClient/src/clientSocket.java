import java.net.*;
import java.io.*;
/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    21-01-2015
 * Time:    16:54
 */
public class clientSocket {

    private Socket client;
    private OutputStream outToServer;
    private DataOutputStream out;
    private InputStream inFromServer;
    private DataInputStream in;

    public void connectTo(String address, int port) {
        try {
            client = new Socket(address,port);
            recieveText();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void disconnectFrom() {
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendText(String[] text) {
        try {
            int l = text.length-1;
            String str = "";
            outToServer = client.getOutputStream();
            out = new DataOutputStream(outToServer);
            for(int i=0;i<l;i++) {
                str += text[i]+" ";
            }
            out.writeUTF(str);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String recieveText() {
        try {
            inFromServer = client.getInputStream();
            in = new DataInputStream(inFromServer);
            return in.readUTF();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
