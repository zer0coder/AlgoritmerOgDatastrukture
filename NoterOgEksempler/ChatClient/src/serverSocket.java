import java.net.*;
import java.io.*;
/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    21-01-2015
 * Time:    16:54
 */
public class serverSocket extends Thread{

    private ServerSocket serverSoc;

    public static void main(String [] args)
    {
        String ttt = "12000";
        int port = Integer.parseInt(ttt);

        //int port = Integer.parseInt(args[0]);

        try
        {
            Thread t = new serverSocket(port);
            t.start();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public serverSocket(int port) throws IOException {
        serverSoc = new ServerSocket(port);
        serverSoc.setSoTimeout(10000);
    }

    public void run(){
        while(true){
            try{

                System.out.println("Waiting for client on port " + serverSoc.getLocalPort() + "...");
                Socket server = serverSoc.accept();
                System.out.println("Just connected to " + server.getRemoteSocketAddress());
                DataInputStream in = new DataInputStream(server.getInputStream());
                System.out.println(in.readUTF());
                DataOutputStream out = new DataOutputStream(server.getOutputStream());
                out.writeUTF("Thank you for connecting to " + server.getLocalSocketAddress());

            } catch (SocketTimeoutException ste){
                System.out.println("Socket timed out!");
                break;
            } catch (IOException ie) {
                ie.printStackTrace();
                break;
            }
        }
    }
}
