import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    17-01-2015
 * Time:    22:36
 */
public class ChatClient {

    private static JFrame frame = new JFrame("Chat Client v0.01pa");

    public static void main(String[] args) {
        // Connecting Area
        JButton conButton = new JButton("Connect");
        JButton disButton = new JButton("Disconnect");
        final JTextField address = new JTextField("127.0.0.1");
        JTextField portNum = new JTextField("12000");
        final clientSocket cs = new clientSocket();

        final int port = 12000;

        // Chat Area
        JButton send = new JButton("Send");
        JTextField textSend = new JTextField();
        final JTextArea textArea = new JTextArea("Default: ");
        textArea.setEnabled(false);

        //Listeners
        conButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cs.connectTo(address.getText(),port);
            }
        });
        disButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cs.disconnectFrom();
            }
        });

        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {
                System.exit(0);
            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });

        //Frame Layout
        frame.setLayout(new GridLayout(3,3));

        frame.add(conButton);
        frame.add(disButton);
        frame.add(address);
        frame.add(portNum);

        frame.add(send);
        frame.add(textSend);
        frame.add(textArea);

        frame.pack();
        frame.setVisible(true);
    }
}
