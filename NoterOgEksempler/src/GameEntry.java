/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukturer
 * Date:    05-01-2015
 * Time:    16:57
 */
public class GameEntry {
    private String name;
    private int score;

    /**
     * Constructor
     * @param n name parameter
     * @param s score parameter
     */
    public GameEntry(String n, int s) {
        name = n;
        score = s;
    }
    public String getName() { return name; }
    public int getScore() { return score; }

    public String toString() {
        return "(" + name + ", " + score + ")";
    }
}
