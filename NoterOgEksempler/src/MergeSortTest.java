import java.util.Arrays;

/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    10-01-2015
 * Time:    14:32
 */
public class MergeSortTest {

    public static void main(String[] args) {
        int[] a = makeArray();
        System.out.println("Array (Unsorted): " + Arrays.toString(a));
        MergeSortInt.mergeSort(a);
        System.out.println("Array (Sorted): " + Arrays.toString(a));
    }

    private static int[] makeArray() {
        int size = 10;
        int []array = new int[size];
        for(int i = 0; i<size; i++) {
            array[i] = (int)(Math.random()*100);
        }
        return array;
    }
}
