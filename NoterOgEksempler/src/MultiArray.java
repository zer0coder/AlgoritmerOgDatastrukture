import java.util.Arrays;

/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    14-01-2015
 * Time:    18:14
 */
public class MultiArray {

    private static int ARRAY_SIZE = 10;
    private static int[]a;
    private static int[]array = new int[(ARRAY_SIZE*ARRAY_SIZE)];

    public static void main(String[] args) {

        int[][] mul = makeMultiArray();

        System.out.println("Unsorted MultiArray of n x n (10): ");
        for(int k=0;k<mul.length;k++) {
            for(int o=0;o<mul.length;o++) {
                System.out.print(mul[k][o] + " ");
            }
            System.out.println();
        }
        multiQS(mul);
        System.out.println();

        System.out.println("Sorted MultiArray of n x n (10): ");
        for(int k=0;k<mul.length;k++) {
            for(int o=0;o<mul.length;o++) {
                System.out.print(mul[k][o] + " ");
            }
            System.out.println();
        }
    }

    private static void multiQS(int[][] mul) {
        int s = 0;
        for(int u=0;u<ARRAY_SIZE;u++){
            for(int b=0;b<ARRAY_SIZE;b++){
                array[s] = mul[u][b];
                s++;
            }
        }
        int left = 0, reset = 0;
        int right = array.length-1;
        a = array;
        quickSort(left,right);
        for(int t=0;t<ARRAY_SIZE;t++){
            for(int q=0;q<ARRAY_SIZE;q++){
                mul[t][q] = array[q+reset];
            }
            reset += 10;
        }
    }

    private static void quickSort(int left, int right) {
        if(left>=right) {
            return;
        }
        int p = a[right];
        int partition = partition(left, right, p);
        quickSort(0,partition-1);
        quickSort(partition + 1, right);
    }

    private static int partition(int left,int right,int p){
        int lc = left-1;
        int rc = right;
        while(lc < rc){
            while(a[++lc] < p);
            while(rc > 0 && a[--rc] > p);
            if(lc >= rc){
                break;
            }else{
                swap(lc, rc);
            }
        }
        swap(lc, right);
        return lc;
    }

    private static void swap(int lc, int right) {
        int temp = a[lc];
        a[lc] = a[right];
        a[right] = temp;
    }

    private static int[][] makeMultiArray() {
        int size = ARRAY_SIZE;
        int[] n = new int[size];
        int[] m = new int[size];
        int[][] result = new int[size][size];

        for(int i=0; i<size;i++) {
            n[i] = (int)(Math.random()*100);
            for(int j=0; j<size;j++) {
                m[j] = (int)(Math.random()*100);
                result[i][j] = m[j];
            }
        }
        return result;
    }
}
