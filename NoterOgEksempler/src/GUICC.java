import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    06-01-2015
 * Time:    14:22
 */
public class GUICC {

    private static int k = 3;
    private static JFrame frame;
    private static String text;

    public static void main(String[] args) {
        frame = new JFrame("Caesar Cipher Test");
        final JTextField textField = new JTextField("TESTING AREA 51");
        JButton encrypt = new JButton("Encrypt");
        JButton decrypt = new JButton("Decrypt");

        final CaesarCipher cc = new CaesarCipher(k);

        encrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                text = cc.encrypt(textField.getText());
                textField.setText(text);
            }
        });
        decrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                text = cc.decrypt(textField.getText());
                textField.setText(text);
            }
        });

        frame.setLayout(new GridLayout());
        frame.add(encrypt);
        frame.add(decrypt);
        frame.add(textField);
        frame.pack();
        frame.setVisible(true);
    }
}
