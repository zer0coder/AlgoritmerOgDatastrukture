import java.util.Arrays;

/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    10-01-2015
 * Time:    15:21
 */
public class QuickSortIntTest {

    public static void main(String[] args) {
        int[] a = makeArray();
        System.out.println("Array (Unsorted): " + Arrays.toString(a));
        QuickSortInt.sort(a);
        System.out.println("Array (Sorted): " + Arrays.toString(a));

    }

    public static int[] makeArray(){
        int size=10;
        int []array = new int[size];
        for(int i=0;i<size;i++){
            array[i] = (int)(Math.random()*100);
        }
        return array;
    }
}
