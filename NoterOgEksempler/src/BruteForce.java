/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    10-01-2015
 * Time:    13:22
 */
public class BruteForce {

    private static String s;
    private static String l;

    public static void main(String[] args) {
        s = "wwertyewertyqwertyqwertyqwetwqe";
        l = "qwerty";

        char[] t = s.toCharArray();
        char[] p = l.toCharArray();

        findBrute(t, p);
    }

    public static int findBrute(char[] text, char[] pattern) {
        int n = text.length;
        int m = pattern.length;

        System.out.print("Text: ");
        System.out.println(text);
        System.out.print("Patter: ");
        System.out.println(pattern);

        for (int i=0; i <= n - m; i++) { // try every starting index within text
            int k = 0; // k is index into pattern
            while (k < m && text[i+k] == pattern[k]) { // kth character of pattern matches
                k++;
            }
            if (k == m) { // if we reach the end of the pattern,
                System.out.println("Found text pattern at [char] index: " + i);
                return i; // substring text[i..i+m-1] is a match
            }
        }
        System.out.println("Found NO text pattern!");
        return -1; // search failed
    }
}
