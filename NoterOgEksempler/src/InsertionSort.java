/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukturer
 * Date:    05-01-2015
 * Time:    17:17
 */
public class InsertionSort {

    public InsertionSort(char[] data) {
        int n = data.length;
        for (int k = 1; k<n; k++) {
            char cur = data[k];
            int j = k;
            while(j>0 && data[j-1] > cur) {
                data[j] = data[j-1];
                j--;
            }
            data[j] = cur;
        }
        System.out.println(data);
    }
}
