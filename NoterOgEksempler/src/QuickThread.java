import java.util.concurrent.*;

/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    14-01-2015
 * Time:    21:09
 */
public class QuickThread implements Runnable{

    public static final int MAX_THREADS = Runtime.getRuntime().availableProcessors();
    static final ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);
    static final int[] my_array;
    final int start, end;
    private static final int minParitionSize;

    public QuickThread(int minParitionSize, int[] array, int start, int end){
        this.minParitionSize = minParitionSize;
        this.my_array = array;
        this.start = start;
        this.end = end;
    }

    public void run() {
        quickSort(my_array, start, end);
    }

    private static void quickSort(int[] array, int left, int right) {
        int len = right-left + 1;

        if (len <= 1)
            return;

        int pivot_index = array[right];
        int pivotValue = array[pivot_index];

        swap(pivot_index, right);

        int storeIndex = left;
        for (int i = left; i < right; i++) {
            if (array[i] <= pivotValue) {
                swap(i, storeIndex);
                storeIndex++;
            }
        }

        swap(storeIndex, right);

        if (len > minParitionSize) {

            QuickThread quick = new QuickThread(minParitionSize, array, left, storeIndex - 1);
            Future<?> future = executor.submit(quick);
            quickSort(array, storeIndex + 1, right);

            try {
                future.get(1000, TimeUnit.SECONDS);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            quickSort(array, left, storeIndex - 1);
            quickSort(array, storeIndex + 1, right);
        }

    }

    private static int partition(int left,int right,int p){
        int lc = left-1;
        int rc = right;
        while(lc < rc){
            while(my_array[++lc] < p);
            while(rc > 0 && a[--rc] > p);
            if(lc >= rc){
                break;
            }else{
                swap(lc, rc);
            }
        }
        swap(lc, right);
        return lc;
    }

    private static void swap(int lc, int right) {
        int temp = my_array[lc];
        my_array[lc] = a[right];
        my_array[right] = temp;
    }
}
