/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    10-01-2015
 * Time:    14:34
 */

/**
 * Simplified version of MergeSort.java class
 */
public class MergeSortInt {
    /**
     * This method takes an integer array and sorts from from lowest to highest.
     * It does this by splitting them up into two and sorts them recursively and
     * then remerges them.
     * @param array
     */
    public static void mergeSort(int[] array) {
        if (array.length > 1) {
            // split array into two halves
            int[] l = left(array);
            int[] r = right(array);

            mergeSort(l);
            mergeSort(r);
            merge(array, l, r);
        }
    }

    /**
     * This method takes the two sorted arrays and inserts them
     * into the original array thereby overwriting it with the
     * sorted version.
     * @param result
     * @param left
     * @param right
     */
    public static void merge(int[] result, int[] left, int[] right) {
        int i1 = 0, i2 = 0; // default index starting point.

        for (int i = 0; i < result.length; i++) {
            if (i2 >= right.length || (i1 < left.length && left[i1] <= right[i2])) {
                result[i] = left[i1];    // take from left
                i1++;
            } else {
                result[i] = right[i2];   // take from right
                i2++;
            }
        }
    }

    public static int[] left(int[] array) {
        int s = array.length / 2;
        int[] left = new int[s];
        for (int i = 0; i < s; i++) {
            left[i] = array[i];
        }
        return left;
    }

    public static int[] right(int[] array) {
        int s1 = array.length / 2;
        int s2 = array.length - s1;
        int[] right = new int[s2];
        for (int i = 0; i < s2; i++) {
            right[i] = array[i + s1];
        }
        return right;
    }
}