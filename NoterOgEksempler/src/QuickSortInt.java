/**
 * Author:  Francisco
 * Project:    AlgoritmerOgDatastrukture
 * Date:    10-01-2015
 * Time:    15:13
 */
public class QuickSortInt {

    private static int[]a;

    public static void sort(int[]array) {
        int left = 0;
        int right = array.length-1;
        a = array;

        quickSort(left,right);
    }

    private static void quickSort(int left, int right) {
        if(left>=right) {
            return;
        }
        int p = a[right];
        int partition = partition(left, right, p);
        quickSort(0,partition-1);
        quickSort(partition+1,right);
    }

    private static int partition(int left,int right,int p){
        int lc = left-1;
        int rc = right;
        while(lc < rc){
            while(a[++lc] < p);
            while(rc > 0 && a[--rc] > p);
            if(lc >= rc){
                break;
            }else{
                swap(lc, rc);
            }
        }
        swap(lc, right);
        return lc;
    }

    private static void swap(int lc, int right) {
        int temp = a[lc];
        a[lc] = a[right];
        a[right] = temp;
    }

}
