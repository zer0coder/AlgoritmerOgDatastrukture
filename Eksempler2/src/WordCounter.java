// File: WordCounter.java
// Program from Section 5.7 to illustrate the use of TreeMaps and Iterators.
// The program opens and reads a file called words.txt. Each line in this
// file should consist of one or more English words separated by spaces.
// The end of each line must not have any extra spaces after the last word.
// The program reads the file and then a table is printed of
// all words and their counts.

import java.util.*;  // Provides TreeMap, Iterator, Scanner
import java.io.*;    // Provides FileReader, FileNotFoundException
/**
 * Author:  COLORADO UNIVERSITY
 * Project:    AlgoritmerOgDatastrukture
 * Date:    23-01-2015
 * Time:    22:57
 */

public class WordCounter
{

    private static String specificFile;

    public static void main(String[] args)
    {
        specificFile = args[0];

        TreeMap<String, Integer> frequencyData = new TreeMap<String, Integer>( );

        readWordFile(frequencyData);
        //printAllCounts(frequencyData);
        writeAllCounts(frequencyData);
    }

    public static int getCount
            (String word, TreeMap<String, Integer> frequencyData)
    {
        if (frequencyData.containsKey(word))
        {  // The word has occurred before, so get its count from the map
            return frequencyData.get(word); // Auto-unboxed
        }
        else
        {  // No occurrences of this word
            return 0;
        }
    }

    public static void printAllCounts(TreeMap<String, Integer> frequencyData)
    {
        System.out.println("-----------------------------------------------");
        System.out.println("Filename: " + specificFile);
        System.out.println("-----------------------------------------------");
        System.out.println("    Occurrences    Word");

        for(String word : frequencyData.keySet( ))
        {
            System.out.printf("%15d    %s\n", frequencyData.get(word), word);
        }

        System.out.println("-----------------------------------------------");
    }

    public static void writeAllCounts(TreeMap<String, Integer> frequencyData) {
        File counted = new File("Occurent.txt");
        try {
            if(counted.exists()) {
                counted.delete();
            }
            counted.createNewFile();
            FileWriter writeTo = new FileWriter(counted);
            writeTo.write("-----------------------------------------------\n");
            writeTo.write("Filename: " + specificFile + "\n");
            writeTo.write("-----------------------------------------------\n");
            writeTo.write("    Occurrences    Word\n");

            for(String word : frequencyData.keySet())
            {
                writeTo.write("         " + frequencyData.get(word) + "         " + word + "\n");
            }
            writeTo.flush();
            writeTo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readWordFile(TreeMap<String, Integer> frequencyData)
    {
        Scanner wordFile;
        String word;     // A word read from the file
        Integer count;   // The number of occurrences of the word

        try
        {
            wordFile = new Scanner(new FileReader(specificFile));
        }
        catch (FileNotFoundException e)
        {
            System.err.println(e);
            return;
        }

        while (wordFile.hasNext( ))
        {
            // Read the next word and get rid of the end-of-line marker if needed:
            word = wordFile.next( );

            // Get the current count of this word, add one, and then store the new count:
            count = getCount(word, frequencyData) + 1;
            frequencyData.put(word, count);
        }
    }

}
